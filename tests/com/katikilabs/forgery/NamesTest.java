package com.katikilabs.forgery;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class NamesTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testNameLength(){
		
		assertFalse("The return value shouldnt be empty", Names.getSurname().isEmpty());
		assertFalse("The return value shouldnt be empty",Names.getFemaleFullName().isEmpty());
		assertFalse("The return value shouldnt be empty",Names.getFemaleName().isEmpty());
		assertFalse("The return value shouldnt be empty",Names.getMaleName().isEmpty());
		assertFalse("The return value shouldnt be empty",Names.getMaleFullName().isEmpty());
		assertFalse("The return value shouldnt be empty",Names.getFullName().isEmpty());
		
	
	}
	
	@Test
	public void testArrayJoinsCorrectly(){
		Object[] array1 = {"John", "Chepe", "Erick"};
		Object[] array2 = {"Peter", "Jude", "Andrew", "Bonzo"};
		
		Object [] expected = {"John", "Chepe", "Erick", "Peter", "Jude", "Andrew", "Bonzo"};
		Object[] actual =  Names.joinArrays(array1, array2);
		
		assertEquals("Bonzo", actual[6]);
		
		assertArrayEquals(expected, actual);
		
		assertEquals(7, actual.length);
		
	}
	
}
