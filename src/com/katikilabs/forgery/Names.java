package com.katikilabs.forgery;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import com.katikilabs.forgery.data.FemaleName;
import com.katikilabs.forgery.data.MaleName;
import com.katikilabs.forgery.data.SurNames;

public class Names {

	//last name, it gives us an african name, eg Onyango, Munga, Maina etc
	public static String getSurname() {

		SurNames[] names = SurNames.values();

		int random = getRandomInt(0, names.length - 1);

		return names[random].toString();
	}

	public static String getMaleName() {

		MaleName[] names = MaleName.values();

		int random = getRandomInt(0, names.length - 1);

		return names[random].toString();
	}
	
	public static String getFemaleName() {

		FemaleName[] names = FemaleName.values();

		int random = getRandomInt(0, names.length - 1);

		return names[random].toString();
	}
	
	public static String getMaleFullName() {

		return String.format("%s %s", getMaleName(), getSurname());
	}
	
	public static String getFemaleFullName() {

		return String.format("%s %s", getFemaleName(), getSurname());
	}
	
	public static int getRandomInt(int min, int max) {
		Random random = new Random();
		
		int randomNo = random.nextInt(max - min + 1) + min;
		
		return randomNo;

	}
	
	//It combines the male and female enums together, convert them to one string array
	private static String getMaleAndFemaleNames(){
		Object[] firstNames =  joinArrays(FemaleName.values(), MaleName.values());
		
		int random = getRandomInt(0, firstNames.length - 1);
		
		return firstNames[random].toString();
	}
	
	// returns full name that is gender agnostic, returns both male and female full names
	public static String getFullName(){
		
		return  String.format("%s %s", getMaleAndFemaleNames(), getSurname());
	}
	
	//combines two object arrays into one object array and returns it
	public static Object[]  joinArrays(Object[] array1, Object [] array2){
		List list = new ArrayList<>(Arrays.asList(array1));
		
		list.addAll(Arrays.asList(array2));
		
		Object [] myArray = list.toArray();
		
		return myArray;
	}

}
