package com.katikilabs.forgery.data;

public enum SurNames {
	Nanzi, Mbwana, Wangoi, Kabuga, Wandera, Kithure, Ndenge, Sampa, 
	Banzi, Amunga, Otieno, Kariuki, Mutisya, Malingi, Sayere, Kabangi,
	Nyale, Kimutai, Kalama, Mashua, Ahmed, Chipawa, Mateso, Zare, Gachare, 
	Makanza, Kitsao, Mwalimu, Kiprop, Bombola, Ramadhan, Chogo, KalamaWashe,
	Mangi, Otiende, Mataza, Kamau, Kioko, Njoroge, Miheso, Saru, Chilango,
	Warui, Museo, Simiu, Kuria, chonga, Mwamuye, Nzomo, Jerotich, Mwadaki,
	Kazungu, Nyamawi, Mutuku, Onyango, Sanza, Rono, Mwaro, Deche, makau, 
	Nyamala, Kai, Munga, Oling, mwangi, Ngetich, kariuki, Muye, Wangare,
	Karani, Rango, Bao, Opiyo, Maina, Dzoro, Kitunyi, Birya, karisa, Budacho,
	Dzambirywa, Chikombe, Chibakuli, Dima, Denyenye, Chiroboto, Chibindo, Katembo,
	Jeptarus, Wanyonyi, Jerop, Kiprono, Nyakobi, Kibabi, Chirindo, Gongo, Mwamsimbi, 
	Kalume, Kanali, Jepngetich, Cherono, Nora, Ngare, Sanje, Mdiso, Koech, Mzungu, 
	Zia, Makupe, Dengere, Mwaringa, Muriuki, Kiti, Mwakulomba, Karorongo, Katana, 
	Chiheso, Babaso, Mazeras, Malavi, Balolo, Biro, Evans, Malongo, Jilo, Kipngetich,
Mwamalumbo, Wari, Simiyu, Jaramogi, Mfalme, Kamotho, Makau, Munyau, Halwale, Komora, Chonga
}
